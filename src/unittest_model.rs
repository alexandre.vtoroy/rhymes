
#![allow(unused_imports)]

use std::collections::HashMap;
use super::model::Model;


#[test]
fn test_transcriptions() {

    let model = Model::new("cambridge.db");

    assert_eq!(model.get_transcription("begin"),     "bɪˈgɪn");
    assert_eq!(model.get_transcription("below"),     "bɪˈləʊ");
    assert_eq!(model.get_transcription("hello"),     "helˈəʊ");
    assert_eq!(model.get_transcription("object"),    "əbˈdʒekt");
    assert_eq!(model.get_transcription("project"),   "prəˈdʒekt");
    assert_eq!(model.get_transcription("we"),        "wiː");
    assert_eq!(model.get_transcription("group"),     "gruːp");
    assert_eq!(model.get_transcription("troop"),     "truːp");
    assert_eq!(model.get_transcription("adjective"), "ˈædʒ.ek.tɪv");

    // not existing words
    assert_eq!(model.get_transcription("subgroup"), "");
}

#[test]
fn test_transcription_symbols() {

    //let model = Model::new("cambridge.db");
    //assert_eq!(model.unique_transcription_symbols(), TRANS_SYMBOLS);
}

pub fn keys(map: &HashMap<usize, Vec<String>>) -> Vec<usize> {
    let mut keys = map.keys().cloned().collect::<Vec<usize>>();
    keys.sort();
    return keys;
}

#[test]
fn test_rhymes() {
    const START: usize = 2;
    const LIMIT: usize = 10;
    let model = Model::new("cambridge.db");

    let rhymes = |x| model.get_rhymes(x, START, LIMIT);


    let r = rhymes("");
    assert_eq!(keys(&r), []);

    let r = rhymes("123");
    assert_eq!(keys(&r), []);

    let r = rhymes("bonjour");
    assert_eq!(keys(&r), []);

    let r = rhymes("hello");
    assert_eq!(keys(&r), [2, 3]);
    assert_eq!(r[&2], vec!["aglow", "ago", "although", "apropos", "below", "bestow", "blow-by-blow", "bravo", "c/o", "cheerio"]);
    assert_eq!(r[&3], vec!["aglow", "below", "blow-by-blow", "go-slow", "hallo", "hullo"]);

    let r = rhymes("group");
    assert_eq!(keys(&r), [2, 3]);
    assert_eq!(r[&2], vec!["cantaloup", "cantaloupe", "coop", "croup", "droop", "dupe", "gloop", "goop", "hoop", "loop"]);
    assert_eq!(r[&3], vec!["croup", "droop", "troop", "troops", "troupe"]);

    let r = rhymes("we");
    assert_eq!(keys(&r), [2]);
    assert_eq!(r[&2], vec!["kiwi", "peewee", "twee", "wee", "wee-wee"]);

    let r = rhymes("clock");
    assert_eq!(keys(&r), [2, 3]);
    assert_eq!(r[&2], vec!["aftershock", "airlock", "ballcock", "bedrock", "bloc", "block", "bloodstock", "chock", "cock", "cocker"]);
    assert_eq!(r[&3], vec!["airlock", "bloc", "block", "deadlock", "fetlock", "flock", "flocking", "forelock", "gridlock", "hemlock"]);

    let r = rhymes("definition");
    assert_eq!(keys(&r), [2, 3, 4]);
    assert_eq!(r[&2], vec!["abolition", "abolitionist", "academician", "acquisition", "addition", "additional", "additionally", "admission", "admissions", "admonishment"]);
    assert_eq!(r[&3], vec!["admonishment", "admonition", "ammo", "ammunition", "clinician", "cognition", "ignition", "initial", "initialize", "initially"]);
    assert_eq!(r[&4], vec!["clinician", "initial", "initialize", "initially"]);
}
