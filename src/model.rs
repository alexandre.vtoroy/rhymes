
use std::cmp::min;
use std::collections::HashMap;
use crate::database::{Database, open_database, NB_TR};
use crate::sound::{calc_suffix, sound_len};


pub struct Model {
    db: Database,
}

impl Model {
    pub fn new(path: &str) -> Model {
        return Model{ db: open_database(path), }
    }

    pub fn get_transcription(&self, word: &str) -> String {
        return self.db.find_first_word(word).transcription;
    }

    pub fn unique_transcription_symbols(&self) -> String {

        let result = self.db.unique_transcription_symbols();
        let mut symbols_string = String::from("");
        match result {
            Ok(mut symbols) => {
                symbols.sort();
                for c in symbols {
                    symbols_string.push(c);
                }
            },
            Err(_err) => (),
        }
        return symbols_string;
    }

    pub fn get_rhymes(&self, word: &str, start: usize, limit: usize) -> HashMap<usize, Vec<String>> {

        let mut result_map: HashMap<usize, Vec<String>> = HashMap::new();
        let item = self.db.find_first_word(word);
        let trans = item.transcription;
        let n = min(sound_len(&trans), NB_TR);
        //println!("find_word = {} {} {}", word, &trans, n);

        for i in (start..=n).rev() {
            let suffix = calc_suffix(&trans, i);
            //println!("suffix = {} {}", i, suffix);
            let result = self.db.find_words_by_tr(i, &suffix, limit, &|x| {
                return !x.ends_with(word);
            });
            match result {
                Ok(rhymes) => {
                    //println!("rhymes = {:?}", rhymes);
                    if rhymes.len() > 0 {
                        result_map.insert(i, rhymes);
                    }
                },
                Err(_err) => (),
            }
        }

        return result_map;
    }
}
