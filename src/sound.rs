
use std::cmp::max;


const TRANS_SPEC     : &str = " -.ˈˌː̩";
const TRANS_VOWEL    : &str = "aeiuæɑɒɔəɜɪʊʌ";
const TRANS_CONSONANT: &str = "bdfghjklmnprstvwzðŋʃʒθ";
const EMPHASIS       : &str = "ˈ";


#[derive(Debug, PartialEq)]
pub enum SoundType {
    Unknown,
    Emphasis,
    Special,
    Vowel,
    Consonant,
}

pub fn sound_type(sound: char) -> SoundType {

    if EMPHASIS.chars().nth(0).unwrap()==sound { return SoundType::Emphasis; }
    if TRANS_SPEC.contains(sound)      { return SoundType::Special;   }
    if TRANS_VOWEL.contains(sound)     { return SoundType::Vowel;     }
    if TRANS_CONSONANT.contains(sound) { return SoundType::Consonant; }

    return SoundType::Unknown;
}

pub fn sound_len(trans: &str) -> usize {

    let mut result: usize = 0;
    for c in trans.chars() {
        match sound_type(c) {
            SoundType::Vowel     => result += 1,
            SoundType::Consonant => result += 1,
            _ => (),
        }
    }
    return result;
}

pub fn calc_suffix(trans: &str, len: usize) -> String {

    let mut parts: Vec<String> = Vec::new();
    let mut is_emph = false;
    let mut n: usize = 0;

    for c in trans.chars() {

        match c {
            'ˌ' => continue,
            '-' => continue,
            _   => (),
        }

        match sound_type(c) {
            SoundType::Emphasis => {
                is_emph = true;
            },
            SoundType::Special => {
                if n==0 {
                    println!("Problem with '{}'", trans);
                    return String::from("");
                }
                parts[n-1].push(c);
            },
            _ => {
                let mut item = String::from("");
                if is_emph && sound_type(c)==SoundType::Vowel {
                    item.push(EMPHASIS.chars().nth(0).unwrap());
                    is_emph = false;
                }
                item.push(c);
                parts.push(item);
                n += 1;
            },
        }
    }
    //println!("parts = {:?}", parts);
    let start = max(len, n) - len;
    return parts[start..n].join("");
}
