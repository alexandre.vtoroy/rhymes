
use rusqlite::{Connection, Transaction};
use std::collections::HashSet;
use std::error::Error;
use crate::sound::calc_suffix;


pub struct Database {
    pub path: String,
    pub conn: Connection,
}


#[derive(Debug)]
pub struct WordItem {
    pub word: String,
    pub transcription: String,
    pub translation: String,
}

impl WordItem {

    pub fn clone(&self) -> WordItem {
        return WordItem {
            word: self.word.clone(),
            transcription: self.transcription.clone(),
            translation: self.translation.clone(),
        };
    }

    pub fn empty() -> WordItem {
        return WordItem {
            word: String::from(""),
            transcription: String::from(""),
            translation: String::from(""),
        };
    }
}



pub const NB_TR: usize = 6;
    const TABLE: &str = "words";


pub fn open_database(path_arg: &str) -> Database {

    let connection = Connection::open(path_arg).unwrap();
    let trs = (1..=NB_TR).map(|x| format!("tr{} CHAR({})", x, x)).collect::<Vec<_>>().join(", ");
    let cmd = format!("CREATE TABLE if not exists {} (\
        word TEXT, transcription TEXT, translation TEXT, {});", TABLE, trs);

    //println!("\t\tTABLE: {}", cmd);
    connection.execute(&cmd, []).unwrap();
    return Database{ conn: connection, path: path_arg.to_string() };
}

pub fn add_word(tx: &Transaction, word: &String, transcription: &String, translation: &String) {

    let values = (1..=NB_TR+3).map(|x| {
        format!(r#""{}""#,
            match x {
                1 => word.to_string(),
                2 => transcription.to_string(),
                3 => translation.to_string(),
                _ => calc_suffix(transcription, x-3),
            })
    }).collect::<Vec<_>>().join(", ");
    let cmd = format!("INSERT INTO {} VALUES({});", TABLE, values);
    //println!("\t\tWORD: {}", cmd);
    tx.execute(&cmd, []).unwrap();
}

pub fn create_indices(tx: &Transaction) {
    for i in 1..=NB_TR {
        let cmd = format!("CREATE INDEX tr{}_index ON {} (tr{});", i, TABLE, i);
        //println!("\t\tINDEX: {}", cmd);
        tx.execute(&cmd, []).unwrap();
    }
}

impl Database {

    pub fn find_word(&self, word: &str) -> Result<Vec<WordItem>, Box<dyn Error>> {

        let cmd = format!("SELECT * FROM {} where word=?", TABLE);
        let mut stmt = self.conn.prepare(&cmd)?;
        let mut rows = stmt.query(rusqlite::params![word])?;
        let mut items = Vec::new();

        while let Some(row) = rows.next()? {

            let item = WordItem {
                word: row.get(0)?,
                transcription: row.get(1)?,
                translation: row.get(2)?
            };
            //println!("{:?}", item);
            items.push(item);
        }
        Ok(items)
    }

    pub fn find_first_word(&self, word: &str) -> WordItem {

        let result = self.find_word(word);
        match result {
            Ok(items) => {
                if items.len() > 0 {
                    return items[0].clone();
                }
            },
            Err(_err) => (),
        }
        return WordItem::empty();
    }

    pub fn unique_transcription_symbols(&self) -> Result<Vec<char>, Box<dyn Error>> {

        let cmd = format!("SELECT * FROM {}", TABLE);
        let mut stmt = self.conn.prepare(&cmd)?;
        let mut rows = stmt.query([])?;
        let mut chars = HashSet::new();

        while let Some(row) = rows.next()? {

            let transcription: String = row.get(1)?;
            for c in transcription.chars() {
                chars.insert(c);
            }
        }

        let mut chars_vector = Vec::new();
        for c in chars {
            chars_vector.push(c);
        }
        Ok(chars_vector)
    }

    pub fn find_words_by_tr(&self, tr_index: usize, tr_value: &str, limit: usize,
        can_be_added: &dyn Fn(&String) -> bool) -> Result<Vec<String>, Box<dyn Error>> {

        let cmd = format!("SELECT word FROM {} where tr{}=?", TABLE, tr_index);
        //println!("{}", cmd);

        let mut stmt = self.conn.prepare(&cmd)?;
        let mut rows = stmt.query(rusqlite::params![tr_value])?;
        let mut items = Vec::new();

        while let Some(row) = rows.next()? {

            let item: String = row.get(0)?;
            //println!("{}", word);
            if can_be_added(&item) {
                items.push(item);
            }
            if items.len() >= limit {
                break;
            }
        }

        Ok(items)
    }
}
