
pub mod database;
pub mod sound;
pub mod stardict;
pub mod utils;

use regex::Regex;
use std::env;
use std::time::Instant;
use crate::database::{open_database, add_word, create_indices};
use crate::stardict::StarDict;
use crate::utils::show_time;


fn show_word(pattern: &Regex, word: &String, transcription: &String, _translation: &String) {

    if pattern.is_match(word) && transcription=="" {
        println!("{}", word);
    }
    //println!("word = {}\ntranscription = {}\ntranslation = {}\n", word, transcription, translation);
}

fn main() {

    // Parse CLI arguments
    let args: Vec<String> = env::args().collect();
    println!("Convert the StarDict dictionary to Transcriptions table");

    let input = &args[1];
    println!("\tInput file: {}", input);
    
    let now = Instant::now();
    let write_db = true;

    // Parse StarDict dictionary
    let sd = StarDict::new();

    if write_db {
        // Connection with database
        let database_path = input.replace(".dict", ".db");
        println!("\tDatabase file: {}", database_path);
        let mut db = open_database(&database_path);

        // Convert the dictionary to a database
        let tx = db.conn.transaction().unwrap();
        sd.open(input, &|word, transcription, translation| {
            if word.len() > 0
                && word.chars().nth(0).unwrap() >= 'a'
                && word.chars().nth(0).unwrap() <= 'z'
                && !word.contains(' ')
                && transcription.len()>0
                //&& translation.len()>0
            {
                add_word(&tx, word, transcription, translation);
            }
        });
        create_indices(&tx);
        tx.commit().unwrap();
    } else {
        let pattern = Regex::new(r"^[a-z]+$").unwrap();
        sd.open(input, &|word, transcription, translation| {
            show_word(&pattern, word, transcription, translation);
        });
    }

    show_time(now);
}
