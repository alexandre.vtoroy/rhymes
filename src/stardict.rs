
use regex::Regex;
use crate::utils::show_count;

pub struct StarDict {}


impl StarDict {
    
    pub fn new() -> StarDict {
        return StarDict{};
    }
    
    pub fn open(&self, file: &String, treat: &dyn Fn(&String, &String, &String)) {

        println!("{}", file);
        let mut data = std::fs::read_to_string(file).expect("Failed to open file");
        data = data.replace(r#"c="rosybrown">strong form  </c><c "#, "");
        let lines = data.split("\n");

        let word_expr = Regex::new(r"<k>([^<>]+)</k>").unwrap();
                                //   <k> WORD </k>
        let transcription_expr = Regex::new(r#"<c c="darkcyan">\[</c><c c="darkcyan">([^<>]+)</c>"#).unwrap();
                                         //    <c c="darkcyan">[</c><c c="darkcyan"> TRANSCRIPTION </c>
        let translation_expr = Regex::new(r#"<blockquote><c c="darkmagenta">(.+)</blockquote>"#).unwrap();
                                       //    <blockquote><c c="darkmagenta"> TRANSLATION </blockquote>

        let expressions: Vec<Regex> = Vec::from([word_expr, transcription_expr, translation_expr]);

        let b_tag = Regex::new(r"<b>.+</b>").unwrap();
        let c_tag = Regex::new(r"<c>.+</c>").unwrap();
        let c_open_tag = Regex::new(r"<c[^<>]+>").unwrap();

        let mut word: String = String::new();
        let mut transcription: String = String::new();
        let mut translation: Vec<String> = Vec::new();

        let mut count = 0;
        let mut limit = 0;
        let delta = 10000;

        for line in lines {
            count += 1;
            if count >= limit {
                limit += delta;
                show_count(count);
            }

            for i in 0..expressions.len() {
                let caps = expressions[i].captures(line);
                if caps.is_some() {
                    let mut value: String = String::from(caps.unwrap().get(1).unwrap().as_str());
                    match i {
                        2 => {
                            value = b_tag.replace_all(&value, "").into_owned();
                            value = c_tag.replace_all(&value, "").into_owned();
                            value = c_open_tag.replace_all(&value, "").into_owned();
                            value = value.replace("</c>", "").trim().to_string();
                        },
                        _ => {},
                    }
                    match i {
                        0 => { 
                            if word.len() > 0 {
                                treat(&word, &transcription, &translation.join("\n"));
                            }
                            word = value.replace("&apos;", "'");
                            transcription = String::new();
                            translation = Vec::new();
                        },
                        1 => { transcription = value; },
                        2 => {
                            value = value.replace("(", "").replace(")", "");
                            if value.len()>0 {
                                translation.push(value);
                            }
                        },
                        _ => {},
                    }
                }
            }
        }

        show_count(count);
        if word.len() > 0 {
            treat(&word, &transcription, &translation.join("\n"));
        }
    }
}
