
use std::io;
use std::io::Write;
use std::time::Instant;


pub fn show_count(count: i32) {
    print!("\r{}...", count);
    io::stdout().flush().unwrap();
}

pub fn show_time(now: Instant) {
    println!("Elapsed time = {} ms", now.elapsed().as_millis());
}
