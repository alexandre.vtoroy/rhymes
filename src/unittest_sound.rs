
#![allow(unused_imports)]

use crate::sound::{calc_suffix, sound_len, sound_type, SoundType};


#[test]
fn test_sound_types() {

    assert_eq!(sound_type('a'), SoundType::Vowel);
    assert_eq!(sound_type('ə'), SoundType::Vowel);

    assert_eq!(sound_type('d'), SoundType::Consonant);
    assert_eq!(sound_type('ʒ'), SoundType::Consonant);

    assert_eq!(sound_type('-'), SoundType::Special);
    assert_eq!(sound_type('.'), SoundType::Special);

    assert_eq!(sound_type('ˈ'), SoundType::Emphasis);

    assert_eq!(sound_type('c'), SoundType::Unknown);
    assert_eq!(sound_type('1'), SoundType::Unknown);
}

#[test]
fn test_sound_lengths() {
    assert_eq!(sound_len(""), 0);
    assert_eq!(sound_len("a"), 1);
    assert_eq!(sound_len("ˈa"), 1);
    assert_eq!(sound_len("helˈəʊ"), 5);
    assert_eq!(sound_len("əbˈdʒekt"), 7);
}

#[test]
fn test_suffixes() {

    let tr = "gruːp";
    assert_eq!(calc_suffix(tr, 0), "");
    assert_eq!(calc_suffix(tr, 1), "p");
    assert_eq!(calc_suffix(tr, 2), "uːp");
    assert_eq!(calc_suffix(tr, 3), "ruːp");
    assert_eq!(calc_suffix(tr, 4), "gruːp");
    assert_eq!(calc_suffix(tr, 5), "gruːp");

    let tr = "ˈædʒ.ek.tɪv";
    assert_eq!(calc_suffix(tr, 0), "");
    assert_eq!(calc_suffix(tr, 1), "v");
    assert_eq!(calc_suffix(tr, 2), "ɪv");
    assert_eq!(calc_suffix(tr, 3), "tɪv");
    assert_eq!(calc_suffix(tr, 4), "k.tɪv");
    assert_eq!(calc_suffix(tr, 5), "ek.tɪv");
    assert_eq!(calc_suffix(tr, 6), "ʒ.ek.tɪv");
    assert_eq!(calc_suffix(tr, 7), "dʒ.ek.tɪv");
    assert_eq!(calc_suffix(tr, 8), "ˈædʒ.ek.tɪv");
    assert_eq!(calc_suffix(tr, 9), "ˈædʒ.ek.tɪv");

    assert_eq!(calc_suffix("bɪˈləʊ", 6), "bɪlˈəʊ");

    assert_eq!(calc_suffix("ˌkəʊ.prəˈdʌk.ʃ", 5), "ədˈʌk.ʃ");
}
